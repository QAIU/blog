---
title: HelloHexo
date: 2019-03-31 00:59:58
tags: hexo
---

hexo填坑记(纯记录非教程)
## 使用cnpm
 ** cnpm install hexo-deployer-git --save**
## 配置github
布署到github需要在本地生成ssh密匙，
然后上传公钥到github进行验证，
按照在配置文件_config.yml填写对应githubpage的地址。
**github布署密匙必须允许读写**

## 常用命令
```
vim _config.yml
hexo clean&&hexo g&&hexo s
hexo g&&hexo s
hexo d
/themes/Yilia/layout/_partial
themes/yilia/
```

## 添加主题 
yilia/next/√

## 配置相册
需要翻墙√

## SEO优化
-  sitemap配置
- Baidu自动推送js导入

## 基本搭建好后开始美化(详细教程以后更新)
    0.主题配置文件的初始化 √
    1.增加评论系统√
    2.显示访客数√
        2.1.使用线上统计比如百度统计√
    3.增加文章字数统计/阅读时长/博客总字数√
    4.彩色标签云
    5.增加可折叠目录
    6.点击特效<心形>√
    7.网站运行时长√
    8.领只看板娘<可以自己制作>√
    9.增加文章版权信息
    10.增强代码高亮显示√
    11.第三方分享平台√
    12.网易云音乐√
    13.日历云 
        https://www.jianshu.com/p/b9665a8e8282
    14.添加返回顶部按钮√
